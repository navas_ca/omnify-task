<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers as CTRL;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register'=>false, 'login' => false]);

Route::get('/', function () {
    return redirect()->route('reservation.create');
});


Route::get('reservation-settings', [CTRL\ReservationSettingController::class, 'settings'])->name('reservation.settings');
Route::patch('reservation-settings', [CTRL\ReservationSettingController::class, 'updateSettings'])->name('reservation.settings.update');

Route::get('reservation/create', [CTRL\ReservationController::class, 'create'])->name('reservation.create');
Route::post('reservation/store', [CTRL\ReservationController::class, 'store'])->name('reservation.store');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
