@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Settings') }}</div>
                <div class="card-body">                    
                    <form action="{{ route('reservation.settings.update') }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="form-group">
                            <label for="">N</label>
                            <input type="text" class="form-control" name="n" value="{{ $settings ? $settings->n : '' }}">
                        </div>
                        <div class="form-group">
                            <label for="">D</label>
                            <select name="d" id="" class="form-control">
                                <option {{ $settings ? ($settings->d == "day" ? "selected" : "") : "" }} value="day">Day</option>
                                <option {{ $settings ? ($settings->d == "week" ? "selected" : "") : "" }} value="week">Week</option>
                                <option {{ $settings ? ($settings->d == "month" ? "selected" : "") : "" }} value="month">Month</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">G</label>
                            <select name="g" id="" class="form-control">
                                <option {{ $settings ? ($settings->g == "individual" ? "selected" : "") : "" }} value="individual">Individual</option>
                                <option {{ $settings ? ($settings->g == "group" ? "selected" : "") : "" }} value="group">Group</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">TZ</label>
                            <input type="text" class="form-control" name="tz" value="{{ $settings ? $settings->tz : '' }}">
                        </div>

                        <button class="btn btn-primary" type="submit">Update</button>

                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>
@endsection
