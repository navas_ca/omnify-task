@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
        
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Settings') }}</div>
                <div class="card-body">                     

                    <form id="reservation-form" action="{{ route('reservation.store') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Users</label>
                            <div class="user-ids-block"></div>
                            <button type="button" onclick="addRow()">Add user</button>
                        </div>                        
                        <div class="form-group">
                            <?php date_default_timezone_set('Asia/Kolkata')?>
                            <label for="">Datetime</label>
                            <input type="text" class="form-control" name="data[reservation_datetime][]" id="datetimepicker" value="{{ date('Y-m-d H:i:s') }}">
                        </div>                        
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </form>

                    <hr>
                    <h4>Response:</h4>
                    <pre class="error-response text-danger"></pre>
                    <pre class="success-response text-success"></pre>

                </div>                
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ url('css/jquery.datetimepicker.min.css') }}"/ >
@endsection

@section('scripts')
    <script src="{{ url('js/jquery.datetimepicker.full.min.js') }}"></script>
    <script type="text/javascript">
        function removeRow(btn) {
            $(btn).parent('.item').remove()
            if ($('.user-ids-block').children().length == 0) {
                addRow()
            }
        }
        function addRow() {
            rowHtml = `<div class="item">  
                <input type="text" class="" name="data[user_ids][]">
                <button onclick="removeRow(this)" type="button">X</button>
            </div>`
            $('.user-ids-block').append(rowHtml)
        }
        addRow()

        $('#reservation-form').submit(function(e){
            $('.error-response').html('')
            $('.success-response').html('')
            e.preventDefault()
            $.post({
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function( data, textStatus, jQxhr ){
                    $('.error-response').html('')
                    $('.success-response').html( JSON.stringify(data, null, 5) );
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    $('.success-response').html('')
                    $('.error-response').html(JSON.stringify(jqXhr.responseJSON, null, 5))
                }
            });
        })
        
        jQuery('#datetimepicker').datetimepicker({format: 'Y-m-d H:i:s'});
    </script>
@endsection
