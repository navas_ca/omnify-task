<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;

    public $fillable = ['user_id', 'reservation_timestamp_utc'];

    public function scopeByUser($qry, $user_id) {
    	return $qry->where('user_id', $user_id);
    }

    public function scopeByUsers($qry, $user_ids) {
    	return $qry->whereIn('user_id', $user_ids);
    }

    public function scopeByTimeBetween($qry, $start_time, $end_time) {
    	return $qry->whereBetween('reservation_timestamp_utc', [$start_time, $end_time]);
    }
}
