<?php

namespace App\Http\Controllers;

use App\Models\ReservationSetting;
use Illuminate\Http\Request;
use \Exception;

class ReservationSettingController extends Controller
{    
    public function settings()
    {
        $settings = ReservationSetting::first();
        return view('reservations.settings')->with(['settings' => $settings]);
    }

    public function updateSettings(Request $request) {
        $request->validate([
            'n' => 'required|numeric|gt:0',
            'd' => 'required|in:day,week,month',
            'g' => 'required|in:individual,group',
            'tz' => 'required|timezone',
        ]);
        try {
            $settings = ReservationSetting::firstOrNew();
            $settings->n = $request->n;
            $settings->d = $request->d;
            $settings->g = $request->g;
            $settings->tz = $request->tz;
            $settings->save();
            session()->flash('status', 'Settings updates successfully.');            
            return back();            
        } 
        catch (Exception $e) {
            return back()->withErrors($e->getMessage());
        }
    }    
}
