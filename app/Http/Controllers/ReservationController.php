<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use App\Models\ReservationSetting;
use Illuminate\Http\Request;
use Validator;
use \Carbon\Carbon;
class ReservationController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reservations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'user_ids' => $request->data['user_ids'],
            'reservation_datetime' => $request->data['reservation_datetime'],
        ];
        $validator = Validator::make($data, [
            "user_ids"    => "required|array|min:1",
            "user_ids.*"  => "required|numeric|distinct|min:1",

            "reservation_datetime"    => "required|array|min:1|max:1",
            "reservation_datetime.*"  => "required|date_format:Y-m-d H:i:s",
        ]);
        if ($validator->fails()) {
            $data = [
                    'data' => $validator->messages()
                ];
            return response()->json($data, 400);
        }
        $settings = ReservationSetting::first();
        if (!$settings) {
            return response()->json(['data' => 'Reservation settings is empty. Please complete the settings to proceed'], 400);
        }
        $reserve_time = new Carbon($data['reservation_datetime'][0], $settings->tz);
        $reserve_time->setTimezone("UTC");
        $reservation_timestamp_utc = $reserve_time->getTimestamp();
        $start_time = new Carbon($data['reservation_datetime'][0],$settings->tz);
        $end_time = new Carbon($data['reservation_datetime'][0],$settings->tz);
        if ($settings->d == 'week') {
            $start_time->startOfWeek();
            $end_time->endOfWeek();
        }
        elseif ($settings->d == 'month') {
            $start_time->startOfMonth();
            $end_time->endOfMonth();
        }
        $start_time->setTime('00','00', '00');
        $end_time->setTime('23','59', '59');
        $start_time_utc = $start_time->setTimezone("UTC")->getTimestamp();
        $end_time_utc = $end_time->setTimezone("UTC")->getTimestamp();
        $reservation_restricted = [];
        $create_reservation = [];
        if ($settings->g == "individual") {
            foreach ($data['user_ids'] as $key => $user) {
                $reservations_qry = Reservation::ByUser($user)
                                            ->ByTimeBetween($start_time_utc, $end_time_utc);
                if ($reservations_qry->count() >= $settings->n) {
                    $reservation_restricted[] = $user;
                }
                else {
                    $create_reservation[] = [
                                    'user_id' => $user,
                                    'reservation_timestamp_utc' => $reservation_timestamp_utc
                                ];
                }                
            }
        }
        else {
            $reservations_qry = Reservation::ByUsers($data['user_ids'])
                                        ->ByTimeBetween($start_time_utc, $end_time_utc);
            if ($reservations_qry->count() + (count($data['user_ids'])) > $settings->n) {
                $reservation_restricted[] = $data['user_ids'];
            }
            else {
                foreach ($data['user_ids'] as $key => $user) {                    
                    $create_reservation[] = [
                                    'user_id' => $user,
                                    'reservation_timestamp_utc' => $reservation_timestamp_utc
                                ];
                }
            }
        }
        if (!empty($reservation_restricted)) {
            $r = [
                    'data' => [
                        'is_booking_restricted' => true,
                        'restricted_user_ids' => $reservation_restricted,
                    ]
                ];                
            return response()->json($r, 400);
        }
        else {
            Reservation::insert($create_reservation);
            return response()->json(['success' => 'Reserved successfully'], 200);
        }
        
    }
}
